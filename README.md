# OpenGLES 2.0 Empty iOS App. There is nothing inside the App #


### App contains UIView and some basic setup for OpenGL ES 2.0 ###

* Use UIView interface
* OpenglView subclass UIView
* Initialize the UIView(frame, bound, etc)
* Create OpenglES context
* Create RenderBuffer
* Create FrameBuffer
* Override the layerClass with CAEAGLLayer (it can be used as an OpenGLES render target)
* Bind/add/attach/associate RenderBuffer to FrameBuffer


### How do I get set up? ###

* Open the App with Xcode and run

