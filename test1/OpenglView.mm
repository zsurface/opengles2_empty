//
//  ViewController.m
//  test1
//
//  Created by cat on 5/4/18.
//  Copyright © 2018 myxcode. All rights reserved.
//

#import "OpenglView.h"
#include "CppAronLib.hpp"
#include "CAronLib.h"


typedef struct{
    float Position[3];
    float Color[4];
} Vertex;

const Vertex Vertices[] = {
    {{1, -1, 0}, {1, 0, 0, 1}},
    {{1,  1, 0}, {0, 1, 0, 1}},
    {{-1, 1, 0}, {0, 0, 1, 1}},
    {{-1, -1, 0}, {0, 0, 0, 1}}
};

// Modify vertices so they are within projection near/far planes
//const Vertex Vertices[] = {
//    {{1, -1, -1}, {1, 0, 0, 1}},
//    {{1, 1, -1}, {0, 1, 0, 1}},
//    {{-1, 1, -1}, {0, 0, 1, 1}},
//    {{-1, -1, -1}, {0, 0, 0, 1}}
//};

//void printMatrix(const int size, const float m[size]){
//    for(int i=0; i<size; i++){
//        if((i + 1) % 4 == 0)
//            printf("[%f]\n",m[i]);
//        else
//            printf("[%f]",m[i]);
//    }
//    printf("--------------------------------------\n");
//}
//
//// r = 4
//// i = 0, 1, 2, 3
//// c = 0, 1, 2, 3
////  [r*i + c] -> [r*c + i]
////  [4*1 + 2] -> [r*2 + 1]
////  [6] -> [9]
////
//
//void transpose(float m[16]){
//    int r = 4;
//    for(int i=0; i<4; i++){
//        for(int j=i; j<4; j++){
//            float tmp = m[r*i + j];
//            m[r*i+j] = m[r*j + i];
//            m[r*j + i] = tmp;
//        }
//    }
//}
//
///*Note: OpenglES, column major matrix
// *m[16] = {
// *           0, 1, 2, 3,
// *           4, 5, 6, 7,
// *           8, 9, 10, 11,
// *           12, 13, 14, 15
// *          };
// *
// * translate model to (x, y, z)
// */
//void translate(float m[16], float x, float y, float z){
//    m[3] = x;
//    m[7] = y;
//    m[11] = z;
//    transpose(m);
//}
//
//void rotateZrow(float m[16], float delta){
//    /*
//     *  sin(delta) -cos(delta) 0 0
//     *  cos(delta)  sin(delta) 0 0
//     *  0           0          1 0
//     *  0           0          0 1
//     */
//    m[0] = sin(delta);
//    m[1] = -cos(delta);
//    m[4] = cos(delta);
//    m[5] = sin(delta);
//    m[10] = 1;
//    m[15] = 1;
//}
//
//void rotateZ(float m[16], float delta){
//    rotateZrow(m, delta);
//    transpose(m);
//}
//
//void rotateYrow(float m[16], float delta){
//    /*
//     *  cos(delta)  0  sin(delta) 0
//     *  0           1  0          0
//     *  -sin(delta) 0  cos(delta) 0
//     *  0           0  0          1
//     */
//    m[0] = cos(delta);
//    m[1] = 0;
//    m[2] = sin(delta);
//    m[4] = 0;
//    m[5] = 1;
//    m[6] = 0;
//    m[8] = -sin(delta);
//    m[9] = 0;
//    m[10] = cos(delta);
//    m[15] = 1;
//}
//void rotateY(float m[16], float delta){
//    rotateYrow(m, delta);
//    transpose(m);
//}
//
//void rotateXrow(float m[16], float delta){
//    /*
//     *  1   0             0          0
//     *  0   cos(delta)   -sin(delta) 0
//     *  0   sin(delta)   cos(delta)  0
//     *  0   0             0          1
//     */
//    m[0] = 1;
//    m[5] = cos(delta);
//    m[6] = -sin(delta);
//    m[9] = sin(delta);
//    m[10] = cos(delta);
//    m[15] = 1;
//}
//
//void rotateX(float m[16], float delta){
//    rotateXrow(m, delta);
//    transpose(m);
//}
//
//// multiply matrix: m3 = m1*m2
//void multiply4f(float m1[16], float m2[16], float m3[16]){
//    for(int i=0; i<4; i++){
//        for(int j=0; j<4; j++){
//            for(int k=0; k<4; k++){
//                m3[4*k + j] += m1[4*k + i] * m2[4*i + j];
//            }
//        }
//    }
//}
//
//void multiply4fcol(float m1[16], float m2[16], float m3[16]){
//    for(int i=0; i<4; i++){
//        for(int j=0; j<4; j++){
//            for(int k=0; k<4; k++){
//                m3[4*k + j] += m1[4*k + i] * m2[4*i + j];
//            }
//        }
//    }
//    transpose(m3);
//}
//
//// 0, 4, 8, 12
//void identity(float m[16]){
//    m[0] = 1.0f;
//    m[5] = 1.0f;
//    m[10] = 1.0f;
//    m[15] = 1.0f;
//}
//void perspectiveMatrix(float m[16], float left, float right,float bottom, float top, float near, float far){
//    float l = left;
//    float r = right;
//    float b = bottom;
//    float t = top;
//    float n = near;
//    float f = far;
//
////    [
////        [2n/r-l, 0,    r+l/r-l,  0]
////        [0,     2n/t-b,t+b/t-b,  0]
////        [0,     0,     n+f/n-f,  2nf/n-f]
////        [0,     0,           -1, 0]
////     ]
//    m[0] = (2*n)/(r-l);
//    m[1] = 0;
//    m[2] = (r+l)/(r-l);
//    m[3] = 0;
//
//    m[4] = 0;
//    m[5] = (2*n)/(t-b);
//    m[6] = (t+b)/(t-b);
//    m[7] = 0;
//
//    m[8] = 0;
//    m[9] = 0;
//    m[10] = (n+f)/(n-f);
//    m[11] = (2*n*f)/(n-f);
//
//    m[12] = 0;
//    m[13] = 0;
//    m[14] = -1;
//    m[15] = 0;
//
//    transpose(m);
//}

float perspectMatrix[16] = {0};
float modelView[16] = {0};

const GLubyte Indices[] = {
    0, 1, 2,
    2, 3, 0
};

@interface OpenglView ()

@end

@implementation OpenglView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setupLayer];
        [self setupContext];
        [self setupRenderBuffer];
        [self setupFrameBuffer];
        [self setupVBO];
        [self compileShaders];
        [self initializeRender];
        //[self render];
        [self setupDisplayLink];
    }
    return self;
}

+(Class) layerClass{
    return [CAEAGLLayer class];
}
-(void)setupLayer{
    _eaglLayer = (CAEAGLLayer*) self.layer;
    _eaglLayer.opaque = YES;
}
-(void)setupContext{
    EAGLRenderingAPI api = kEAGLRenderingAPIOpenGLES2;
    _context = [[EAGLContext alloc] initWithAPI:api];
    if(!_context){
        NSLog(@"Fail to initialize OpengGLES 2.0 context");
        exit(1);
    }
    if(![EAGLContext setCurrentContext:_context]){
        NSLog(@"Fail to set current OpenGLES context");
        exit(1);
    }
}

-(void)setupRenderBuffer{
    glGenBuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:_eaglLayer];
}

-(void)setupFrameBuffer{
    GLuint frameBuffer;
    glGenFramebuffers(1, &frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorRenderBuffer);
}

-(void)initializeRender{
    float h = 4.0f*self.frame.size.height/self.frame.size.width;
    _left = -2;
    _right = 2;
    _bottom = -h/2;
    _top = h/2;
    _near = 4;
    _far = 10;
    perspectiveMatrix(perspectMatrix, _left, _right, _bottom, _top, _near, _far);
    
    identity(modelView);
    translate(modelView, 0, 0, -7);
}

-(void)setupDisplayLink{
    CADisplayLink* displayLink = [CADisplayLink displayLinkWithTarget:self
                                                             selector:@selector(render:)];
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

// clear the screen
-(void)render:(CADisplayLink*) displayLink{
    // background color
    glClearColor(0, 104.0/255.0, 55.0/255.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    
    _cftime += displayLink.duration;
    printf("cftime=[%f]\n", _cftime);
    
    rotateZ(modelView, _cftime);
    
    glUniformMatrix4fv(_projectionUniform, 1, GL_FALSE, perspectMatrix);
    glUniformMatrix4fv(_modelViewUniform, 1, GL_FALSE, modelView);
    printMatrix(16, modelView);
    
    glViewport(0, 0, self.frame.size.width, self.frame.size.height);
    glVertexAttribPointer(_positionSlot, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
    glVertexAttribPointer(_colorSlot, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(sizeof(float)*3));
    glDrawElements(GL_TRIANGLES, sizeof(Indices)/sizeof(Indices[0]), GL_UNSIGNED_BYTE, 0);
    [_context presentRenderbuffer:GL_RENDERBUFFER];
}

-(GLuint)compileShader:(NSString*)shaderName withType:(GLenum)shaderType{
    NSString* shaderPath = [[NSBundle mainBundle] pathForResource:shaderName ofType:@"glsl"];
    NSError* error;
    NSString* shaderString = [NSString stringWithContentsOfFile:shaderPath encoding:NSUTF8StringEncoding error:&error];
    if(!shaderString){
        NSLog(@"Error loading shader: %@", error.localizedDescription);
        exit(1);
    }
    GLuint shaderHandle = glCreateShader(shaderType);
    
    const char* shaderStringUTF8 = [shaderString UTF8String];
    NSUInteger shaderStringLength = [shaderString length];
    glShaderSource(shaderHandle, 1, &shaderStringUTF8, &shaderStringLength);
    glCompileShader(shaderHandle);
    
    GLint compileSuccess;
    glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compileSuccess);
    if(compileSuccess == GL_FALSE){
        GLchar messages[256];
        glGetShaderInfoLog(shaderHandle, sizeof(messages), 0, &messages[0]);
        NSString* messageString = [NSString stringWithUTF8String:messages];
        NSLog(@"%@", messageString);
        exit(1);
    }
    return shaderHandle;
}

-(void)compileShaders{
    GLuint vertexShader = [self compileShader:@"SimpleVertex" withType:GL_VERTEX_SHADER];
    GLuint fragmentShader = [self compileShader:@"SimpleFragment" withType:GL_FRAGMENT_SHADER];

    GLuint programHandle = glCreateProgram();
    glAttachShader(programHandle, vertexShader);
    glAttachShader(programHandle, fragmentShader);
    glLinkProgram(programHandle);
    
    GLint linkSuccess;
    glGetProgramiv(programHandle, GL_LINK_STATUS, &linkSuccess);
    if(linkSuccess == GL_FALSE){
        GLchar messages[256];
        glGetProgramInfoLog(programHandle, sizeof(messages), 0, &messages[0]);
        NSString *messageString = [NSString stringWithUTF8String:messages];
        NSLog(@"%@", messageString);
        exit(1);
    }
    glUseProgram(programHandle);
    
    _positionSlot = glGetAttribLocation(programHandle, "Position");
    _colorSlot = glGetAttribLocation(programHandle, "SourceColor");
    glEnableVertexAttribArray(_positionSlot);
    glEnableVertexAttribArray(_colorSlot);

    _projectionUniform = glGetUniformLocation(programHandle, "Projection");
    _modelViewUniform = glGetUniformLocation(programHandle, "Modelview");
}

-(void)setupVBO{
    GLuint vertexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);
    
    GLuint indexBuffer;
    glGenBuffers(1, &indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);
}
@end
