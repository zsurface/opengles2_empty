//
//  CAronLib.h
//  test1
//
//  Created by cat on 5/14/18.
//  Copyright © 2018 myxcode. All rights reserved.
//

#ifndef CAronLib_h
#define CAronLib_h

#include <stdio.h>
#include <math.h>

void printMatrix(const int size, const float m[size]){
    for(int i=0; i<size; i++){
        if((i + 1) % 4 == 0)
            printf("[%f]\n",m[i]);
        else
            printf("[%f]",m[i]);
    }
    printf("--------------------------------------\n");
}

// r = 4
// i = 0, 1, 2, 3
// c = 0, 1, 2, 3
//  [r*i + c] -> [r*c + i]
//  [4*1 + 2] -> [r*2 + 1]
//  [6] -> [9]
//

void transpose(float m[16]){
    int r = 4;
    for(int i=0; i<4; i++){
        for(int j=i; j<4; j++){
            float tmp = m[r*i + j];
            m[r*i+j] = m[r*j + i];
            m[r*j + i] = tmp;
        }
    }
}

/*Note: OpenglES, column major matrix
 *m[16] = {
 *           0, 1, 2, 3,
 *           4, 5, 6, 7,
 *           8, 9, 10, 11,
 *           12, 13, 14, 15
 *          };
 *
 * translate model to (x, y, z)
 */
void translate(float m[16], float x, float y, float z){
    m[3] = x;
    m[7] = y;
    m[11] = z;
    transpose(m);
}

void rotateZrow(float m[16], float delta){
    /*
     *  sin(delta) -cos(delta) 0 0
     *  cos(delta)  sin(delta) 0 0
     *  0           0          1 0
     *  0           0          0 1
     */
    m[0] = sinf(delta);
    m[1] = -cosf(delta);
    m[4] = cosf(delta);
    m[5] = sinf(delta);
    m[10] = 1;
    m[15] = 1;
}

void rotateZ(float m[16], float delta){
    rotateZrow(m, delta);
    transpose(m);
}

void rotateYrow(float m[16], float delta){
    /*
     *  cos(delta)  0  sin(delta) 0
     *  0           1  0          0
     *  -sin(delta) 0  cos(delta) 0
     *  0           0  0          1
     */
    m[0] = cos(delta);
    m[1] = 0;
    m[2] = sin(delta);
    m[4] = 0;
    m[5] = 1;
    m[6] = 0;
    m[8] = -sin(delta);
    m[9] = 0;
    m[10] = cos(delta);
    m[15] = 1;
}
void rotateY(float m[16], float delta){
    rotateYrow(m, delta);
    transpose(m);
}

void rotateXrow(float m[16], float delta){
    /*
     *  1   0             0          0
     *  0   cos(delta)   -sin(delta) 0
     *  0   sin(delta)   cos(delta)  0
     *  0   0             0          1
     */
    m[0] = 1;
    m[5] = cos(delta);
    m[6] = -sin(delta);
    m[9] = sin(delta);
    m[10] = cos(delta);
    m[15] = 1;
}

void rotateX(float m[16], float delta){
    rotateXrow(m, delta);
    transpose(m);
}

// multiply matrix: m3 = m1*m2
void multiply4f(float m1[16], float m2[16], float m3[16]){
    for(int i=0; i<4; i++){
        for(int j=0; j<4; j++){
            for(int k=0; k<4; k++){
                m3[4*k + j] += m1[4*k + i] * m2[4*i + j];
            }
        }
    }
}

void multiply4fcol(float m1[16], float m2[16], float m3[16]){
    for(int i=0; i<4; i++){
        for(int j=0; j<4; j++){
            for(int k=0; k<4; k++){
                m3[4*k + j] += m1[4*k + i] * m2[4*i + j];
            }
        }
    }
    transpose(m3);
}

// 0, 4, 8, 12
void identity(float m[16]){
    m[0] = 1.0f;
    m[5] = 1.0f;
    m[10] = 1.0f;
    m[15] = 1.0f;
}
void perspectiveMatrix(float m[16], float left, float right,float bottom, float top, float near, float far){
    float l = left;
    float r = right;
    float b = bottom;
    float t = top;
    float n = near;
    float f = far;
    
    //    [
    //        [2n/r-l, 0,    r+l/r-l,  0]
    //        [0,     2n/t-b,t+b/t-b,  0]
    //        [0,     0,     n+f/n-f,  2nf/n-f]
    //        [0,     0,           -1, 0]
    //     ]
    m[0] = (2*n)/(r-l);
    m[1] = 0;
    m[2] = (r+l)/(r-l);
    m[3] = 0;
    
    m[4] = 0;
    m[5] = (2*n)/(t-b);
    m[6] = (t+b)/(t-b);
    m[7] = 0;
    
    m[8] = 0;
    m[9] = 0;
    m[10] = (n+f)/(n-f);
    m[11] = (2*n*f)/(n-f);
    
    m[12] = 0;
    m[13] = 0;
    m[14] = -1;
    m[15] = 0;
    
    transpose(m);
}



#endif /* CAronLib_h */
