//
//  OpenglView.h
//  test1
//
//  Created by cat on 5/4/18.
//  Copyright © 2018 myxcode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>

@interface OpenglView : UIView{
    CAEAGLLayer* _eaglLayer;
    EAGLContext* _context;
    GLuint _colorRenderBuffer;
    GLint _positionSlot;
    GLint _colorSlot;
    GLuint _projectionUniform;
    GLuint _modelViewUniform;
    float _left;
    float _right;
    float _top;
    float _bottom;
    float _near;
    float _far;
    CFTimeInterval _cftime;
}

@end

