//
//  AppDelegate.h
//  test1
//
//  Created by cat on 5/4/18.
//  Copyright © 2018 myxcode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenglView.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    OpenglView* _glView;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) OpenglView *glView;

@end

